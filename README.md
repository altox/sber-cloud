# Running server
```bash
docker-compose -f docker-compose-dev.yaml up
```

Docs will be available at http://localhost:9000/api-docs

Default super user will be created automatically: 

login: ADMIN
password: SUPER_SECRET_PASSWORD

# API

API uses HTTP as transport, it's not quite REST, here are some core points:

- requests are split in two types: mutation and query
- query requests only retrieve data and don't mutate any data on server
- mutation requests mutate data
- all API calls use HTTP POST method
- arguments are passed as serialized JSON object
- responses are JSON too

This approach allows us to change transport easily in future (e.g. Websockets).

# Authorization

Auth token is some string returned by a server to client through auth process.
Token should be stored safely on client side.

We don't use JWT because author believes that JWS has no sense, the main idea of JWT was to store session data inside token, so server save reads from a database, but it turns out that any token can be revoked at any time by the client, so we always must check whether provided token is legal in the database.  

Auth token should be passed to X-Sber-Auth header

# Errors

Errors returned as JSON according to structure below

```json
{
  "ok": false,
  "id": "request identifier, could be stored on client side to trace back logs for curtain error on server",
  "errorCode": "some error code here",
  "errorText": "optional string that describes error",
  "shouldRetry": "boolean, whether or not client can retry this request"
}    
```

Possible error codes: 

- rate_limit (client achieved rate limit, request can be retried)
- server_error (some internal server error)
- access_denied (probably you forgot to pass auth token, or token was revoked)
- not_found (some resource required to execute resource was not found)

# Identifiers

We don't expose integer id's straight from database in case of bruteforce attacks.
Instead, we use encrypted string id's. 
This also helps in security since id's are unique and can't be guessed on client side.

# Limitations

In order to keep server safe from overloading we have some limitations per client IP.
Basically client have sliding window of 100/3 ms in which he can make 64 * 3 requests, window is refilled with 30 request every 100/ms.
Those parameters can be changed in concurrency.ts file.

