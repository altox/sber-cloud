FROM node:14.16.0
WORKDIR /app
ADD package.json .
ADD yarn.lock .
RUN yarn
ADD . .

RUN yarn build

CMD ["yarn", "start"]