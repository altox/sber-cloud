import {DataTypes, Model, Optional} from "sequelize";
import {DB} from "../db/initDatabase";
import {AuthToken} from "../auth/AuthToken.model";

export type UserRole =
    'user' |        // Basic user, can auth
    'customer' |    // Customer, can make orders
    'manager' |     // Manager, can manage orders
    'super-user'    // Super user, can

export interface UserInput {
    id: number
    firstName: string
    lastName: string
    userName: string
    role: UserRole

    passwordHash: string
}

export class User extends Model<UserInput, Optional<UserInput, 'id'>> implements UserInput {
    id!: number
    firstName!: string
    lastName!: string
    userName!: string
    role!: UserRole

    passwordHash!: string

    readonly createdAt!: Date
    readonly updatedAt!: Date
}

export function initUserModel() {
    User.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            firstName: {
                type: new DataTypes.STRING(128),
                allowNull: false,
            },
            lastName: {
                type: new DataTypes.STRING(128),
                allowNull: false,
            },
            userName: {
                type: new DataTypes.STRING(128),
                allowNull: false,
                unique: true,
            },
            role: {
                type: new DataTypes.ENUM('user', 'super-user'),
                allowNull: false
            },
            passwordHash: {
                type: new DataTypes.STRING(64),
                allowNull: false
            },
        },
        {
            tableName: "users",
            sequelize: DB,
            indexes: [
                {
                    unique: true,
                    fields: ['userName']
                }
            ]
        }
    )

    User.hasMany(AuthToken)
}