import {Transaction} from "sequelize";
import {User} from "./User.model";
import {inTx} from "../db/inTx";
import {sha256} from "../utils/cryto";
import {NotFoundError} from "../api/errors";

type UserInput = {
    firstName: string
    lastName: string
    userName: string
    password: string
}

export class UsersModule {
    start = async () => {
        await this.createFirstSuperUser()
    }

    createUser = async (tx: Transaction, input: UserInput) => {
        return await User.create({
            firstName: input.firstName,
            lastName: input.lastName,
            userName: input.userName,
            passwordHash: sha256(input.password),
            role: 'user'
        }, { transaction: tx })
    }

    createSuperUser = async (tx: Transaction, input: UserInput) => {
        return await User.create({
            firstName: input.firstName,
            lastName: input.lastName,
            userName: input.userName,
            passwordHash: sha256(input.password),
            role: 'super-user'
        }, { transaction: tx })
    }

    createFirstSuperUser = async () => {
        await inTx(async tx => {
            let existing = await User.findOne({ where: { userName: 'admin' }, transaction: tx })
            if (existing) {
                return
            }
            await this.createSuperUser(tx, {
                firstName: 'ADMIN',
                lastName: '',
                userName: 'admin',
                password: 'SUPER_SECRET_PASSWORD'
            })
        })
    }

    fetchUserPermissions = async (uid: number) => {
        let user = await User.findByPk(uid)
        if (!user) {
            throw new NotFoundError()
        }

        if (user.role === 'user') {
            return ['user']
        } else if (user.role === 'customer') {
            return ['user', 'customer']
        } else if (user.role === 'manager') {
            return ['user', 'customer', 'manager']
        } else if (user.role === 'super-user') {
            return ['user', 'customer', 'manager', 'super-user']
        }

        return []
    }
}