import {APISchema, mutationHandler, queryHandler, withAuth, withPermission} from "../api/queryHandler";
import {BoolResponse, CommonCodecs, SingleIDArgs} from "../api/commonCodecs";
import {resolveUser, UserCodec} from "./codecs";
import {User, UserRole} from "./User.model";
import {NotFoundError, UserError} from "../api/errors";
import {withNamespace} from "../api/withAPIVersion";
import * as t from "io-ts";
import {Modules} from "../modules/Modules";
import {IDs} from "../api/IDs";
import {inTx} from "../db/inTx";

const MyPermissionsResponse = t.type({ permissions: t.array(t.string) })

export const SetUserRoleArgs = t.type({
    id: t.string,
    role: t.string
})

export const Handlers: APISchema =  withNamespace(1, 'users', {
    // Queries
    getMe: queryHandler(CommonCodecs, UserCodec, withAuth(async (ctx, args) => {
        let user = await User.findByPk(ctx.uid)
        if (!user) {
            throw new NotFoundError()
        }
        return resolveUser(user)
    })),
    getMyPermissions: queryHandler(CommonCodecs, MyPermissionsResponse, withAuth(async (ctx, args) => {
        return {
            permissions: await Modules.Users.fetchUserPermissions(ctx.uid)
        }
    })),

    // Mutation
    setUserRole: mutationHandler(SetUserRoleArgs, BoolResponse, withPermission('super-user', async (ctx, args) => {
        return await inTx(async tx => {
            // Validate role
            if (!['user', 'customer', 'manager', 'super-user'].includes(args.role)) {
                throw new UserError('Unknown role ' + args.role)
            }

            let user = await User.findByPk(IDs.User.parse(args.id), { transaction: tx })
            if (!user) {
                throw new NotFoundError()
            }

            user.role = args.role as UserRole
            await user.save({ transaction: tx })

            return { ok: true }
        })
    })),
})