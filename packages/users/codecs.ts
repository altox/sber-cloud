import * as t from "io-ts";
import {User} from "./User.model";
import {IDs} from "../api/IDs";

export const UserCodec = t.type({
    id: t.string,
    username: t.string,
    firstName: t.string,
    lastName: t.string,
})

export const resolveUser = (user: User) => ({
    id: IDs.User.serialize(user.id),
    firstName: user.firstName,
    lastName: user.lastName,
    username: user.userName
})