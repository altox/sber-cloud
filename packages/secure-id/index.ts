//
//  body = [int8 version, int16 typeid, int32 ID]
//  id = aes128(body)
//

import {createCipheriv, createDecipheriv, createHash, pbkdf2Sync} from "crypto";
import Hashids from "hashids";

const TYPE_SALT = '6539fee550954aa4aa67d5f5b3193252865c5ccd29a314f8745c7a15917ec283'
const CRYPTO_KEY_SALT = 'bf8e8a108267bf2a2b9cd0b7e538c5d53337909ef846c3b946bbe9778c293969'
const CRYPTO_IV_SALT = '9d00fc3e61bb211ffda0d4b980691334f5f43403d6e78ba4e1a5f54ac11c2425'
const HASHIDS_SALT = '6fa66323934fcc2ffadb61a4ee76d4b26ef7a959c129d057a24b51514b9a682b'

const ID_VERSION = 1
const UINT_32_MAX = 2 ** 32 - 1
const ID_LENGTH = 7

function encodeBody(typeId: number, idValue: number) {
    if (idValue < 0) {
        throw new Error(`Ids can't be negative`)
    }
    if (!Number.isInteger(idValue)) {
        throw new Error(`Ids should be integer`)
    }
    if (idValue > UINT_32_MAX) {
        throw new Error(`Ids cant be bigger uint32, got: ${idValue}`)
    }

    let buf = Buffer.alloc(7)

    buf.writeUInt8(ID_VERSION, 0)
    buf.writeUInt16BE(typeId, 1)
    buf.writeUInt32BE(idValue, 3)

    return buf
}

function decodeBody(typeId: number, body: Buffer) {
    if (body.byteLength !== ID_LENGTH) {
        throw new Error(`Invalid id`)
    }
    let version = body.readUInt8(0)

    if (version !== ID_VERSION) {
        throw new Error(`Invalid id`)
    }

    if (typeId !== body.readUInt16BE(1)) {
        throw new Error(`Invalid id`)
    }

    return body.readUInt32BE(3)
}

export class SecId {
    constructor(
        private typeId: number,
        private cryptoKey: Buffer,
        private cryptoIv: Buffer,
        private hashids: Hashids
    ) {

    }

    parse(value: string) {
        // decode hashid
        let encrypted = Buffer.from(this.hashids.decodeHex(value), 'hex')

        // decrypt
        let dechiper = createDecipheriv('aes-128-ctr', this.cryptoKey, this.cryptoIv)
        let decrypted = Buffer.concat([dechiper.update(encrypted), dechiper.final()])

        // decode
        return decodeBody(this.typeId, decrypted)
    }

    serialize(value: number) {
        // encode body
        let body = encodeBody(this.typeId, value)

        // encrypt body
        let cipher = createCipheriv('aes-128-ctr', this.cryptoKey, this.cryptoIv)
        let res = cipher.update(body)
        res = Buffer.concat([res, cipher.final()])

        // prettify
        return this.hashids.encodeHex(res.toString('hex'))
    }
}

export class SecIdFactory {
    private knownTypes = new Set<number>()
    private typeSalt: string
    private cryptoKey: Buffer
    private cryptoIv: Buffer
    private hashids: Hashids

    constructor(secret: string) {
        this.typeSalt = pbkdf2Sync(secret, TYPE_SALT, 100, 32, 'sha512').toString('hex')
        this.cryptoKey = pbkdf2Sync(secret, CRYPTO_KEY_SALT, 100, 16, 'sha512')
        this.cryptoIv = pbkdf2Sync(secret, CRYPTO_IV_SALT, 100, 16, 'sha512')
        this.hashids = new Hashids(pbkdf2Sync(secret, HASHIDS_SALT, 100, 32, 'sha512').toString('hex'))
    }

    createId(type: string) {
        let hash = createHash('sha1')
            .update(this.typeSalt)
            .update(type.toLowerCase())
            .digest()

        let typeId = hash.readUInt16BE(0)

        if (this.knownTypes.has(typeId)) {
            throw Error(`SeqID collision, rename your id type ${type}`)
        }
        this.knownTypes.add(typeId)

        return new SecId(
            typeId,
            this.cryptoKey,
            this.cryptoIv,
            this.hashids
        )
    }
}