import {findAllFiles} from "../utils/findAllFiles";
import {AnyQueryHandlerSchema, APISchemaHolder} from "./queryHandler";

export function buildHandlers(rootPath: string) {
    let handlers = findAllFiles(rootPath, src => src.endsWith('.api.js'))
            .map(path => require(path).Handlers)

    let res = new Map<string, AnyQueryHandlerSchema>()

    let knownNames = new Set<string>()

    for (let handler of handlers) {
        let apiVersion: number|undefined
        let namespace: string|undefined

        if (handler instanceof APISchemaHolder) {
            namespace = handler.namespace || undefined
            apiVersion = handler.version
            handler = handler.schema
        }

        for (let key of Object.keys(handler)) {
            if (knownNames.has(key)) {
                throw new Error(`Handler ${key} was already declared, try using other name`)
            }
            res.set(key, { apiVersion, namespace, handler: handler[key] })
            knownNames.add(key)
        }
    }

    return res
}