import {APISchema, queryHandler} from "./queryHandler";
import {CommonCodecs} from "./commonCodecs";
import * as t from "io-ts";
import {withAPIVersion} from "./withAPIVersion";

const HealthCheckRes = t.type({ ok: t.boolean })

export const Handlers: APISchema = withAPIVersion(1, {
    healthCheck: queryHandler(CommonCodecs, HealthCheckRes, async () => ({ ok: true }))
})