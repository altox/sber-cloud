import * as t from "io-ts";

export const CommonCodecs = t.type({ })

export const BoolResponse = t.type({ ok: t.boolean })

export const SingleIDArgs = t.type({ id: t.string })

export const PaginationArgs = t.type({
    first: t.number,
    after: t.union([t.string, t.undefined])
})

export const paginatedResponse = (responseType: t.TypeC<any>) => t.type({
    items: t.array(responseType),
    cursor: t.union([t.string, t.undefined])
})