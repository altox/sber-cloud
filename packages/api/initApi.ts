import express from 'express'
import {buildHandlers} from "./buildHandlers";
import {initHandlers} from "./initHandlers";
import * as swagger from 'swagger-ui-express'
import {buildOpenApiSchema} from "./buildOpenApiSchema";

export async function initApi() {
    const app = express()
    const port = 9000

    app.enable('trust proxy')

    app.get('/ping', (req, res) => {
        res.json({ res: 'pong' })
    })

    const apiSchema = buildHandlers(__dirname + '/../')

    // DOCS
    const openApiSchema = buildOpenApiSchema(apiSchema)
    app.use('/api-docs', swagger.serve, swagger.setup(openApiSchema))

    // API
    initHandlers(app, apiSchema)

    app.listen(port,() => {
        console.log('HTTP API started at port: ' + port)
    })
}