import {APISchemaDefinition, APISchemaHolder} from "./queryHandler";

export function withAPIVersion(version: number, schema: APISchemaDefinition) {
    return new APISchemaHolder(version, null, schema)
}

export function withNamespace(version: number, namespace: string, schema: APISchemaDefinition) {
    return new APISchemaHolder(version, namespace, schema)
}