import * as t from 'io-ts'
import {APIContext, APIContextWithAuth} from "./Context";
import {AccessDeniedError} from "./errors";
import {User, UserRole} from "../users/User.model";
import {Modules} from "../modules/Modules";

export type BaseQueryHandler<Args extends t.TypeC<any>, Res extends t.TypeC<any>> = {
    argsType: Args,
    resType: Res,
    handler: (ctx: APIContext, args: t.OutputOf<Args>) => Promise<t.TypeOf<Res>>
}

export type AnyQueryHandler = BaseQueryHandler<any, any>

export type AnyQueryHandlerSchema = { apiVersion?: number, namespace?: string, handler: BaseQueryHandler<any, any> }

export const buildHandlerPath = (name: string, handler: AnyQueryHandlerSchema, defaultVersion: number) => {
    let version = handler.apiVersion || defaultVersion

    if (handler.namespace) {
        return `/api/v${version}/${handler.namespace}.${name}`
    } else {
        return `/api/v${version}/${name}`
    }
}

export class APISchemaHolder {
    constructor(
        readonly version: number,
        readonly namespace: string|null,
        readonly schema: APISchemaDefinition
    ) {

    }
}

export type APISchemaDefinition = { [key: string]: AnyQueryHandler }

export type APISchema = APISchemaHolder | APISchemaDefinition

export function queryHandler<Args extends t.TypeC<any>, Res extends t.TypeC<any>>(argsType: Args, resType: Res, handler: (ctx: APIContext, args: t.OutputOf<Args>) => Promise<t.TypeOf<Res>>) {
    return {
        type: 'query',
        argsType,
        resType,
        handler
    }
}

export function mutationHandler<Args extends t.TypeC<any>, Res extends t.TypeC<any>>(argsType: Args, resType: Res, handler: (ctx: APIContext, args: t.OutputOf<Args>) => Promise<t.TypeOf<Res>>) {
    return {
        type: 'mutation',
        argsType,
        resType,
        handler
    }
}

export function withAuth<Args, Res>(handler: (ctx: APIContextWithAuth, args: Args) => Promise<Res>): (ctx: APIContext, args: Args) => Promise<Res> {
    return async (ctx, args) => {
        if (ctx.uid === undefined || ctx.tid == undefined) {
            throw new AccessDeniedError()
        }

        // wtf, TS?
        let ctxWithAuth = { ...ctx, uid: ctx.uid!, tid: ctx.tid! }

        return await handler(ctxWithAuth, args)
    }
}

export function withPermission<Args, Res>(permission: UserRole, handler: (ctx: APIContextWithAuth, args: Args) => Promise<Res>): (ctx: APIContext, args: Args) => Promise<Res> {
    return async (ctx, args) => {
        // Check auth
        if (ctx.uid === undefined || ctx.tid == undefined) {
            throw new AccessDeniedError()
        }

        let userPermissions = await Modules.Users.fetchUserPermissions(ctx.uid)
        if (!userPermissions.includes(permission)) {
            throw new AccessDeniedError()
        }

        // wtf, TS?
        let ctxWithAuth = { ...ctx, uid: ctx.uid!, tid: ctx.tid! }

        return await handler(ctxWithAuth, args)
    }
}
