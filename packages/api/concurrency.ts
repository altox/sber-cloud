import {CachedFactory} from "../utils/CachedFactory";
import {TokenBucket} from "../utils/TokenBucket";

export const Concurrency = {
    // Request rate limit, unique per user IP
    Request: new CachedFactory(() => new TokenBucket({
        capacity: 64 * 3,
        refillDelay: 100 / 3,
        refillAmount: 10 * 3
    }))
}