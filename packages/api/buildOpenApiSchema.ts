import {AnyQueryHandlerSchema, buildHandlerPath} from "./queryHandler";
import {ApiErrorResponse, DEFAULT_API_VERSION} from "./initHandlers";
import * as t from "io-ts";

export function buildOpenApiSchema(schema: Map<string, AnyQueryHandlerSchema>) {
    let openApiSchema = {
        openapi: '3.0.0',
        info: {
            title: 'Sber cloud API',
            version: '1.0.0',
            description: 'Sber cloud api documentation'
        },
        servers: [{ url: 'http://localhost:9000'}],
        paths: { }
    }

    for (let [name, handler] of schema.entries()) {
        openApiSchema.paths[buildHandlerPath(name, handler, DEFAULT_API_VERSION)] = {
            post: {
                requestBody: {
                    content: {
                        "application/json": {
                            schema: codec2schema(handler.handler.argsType)
                        }
                    }
                },
                responses: {
                    default: {
                        content: {
                            "application/json": {
                                schema: codec2schema(t.type({
                                    ok: t.boolean,
                                    response: handler.handler.resType
                                }))
                            }
                        }
                    },
                    error: {
                        content: {
                            "application/json": {
                                schema: codec2schema(ApiErrorResponse)
                            }
                        }
                    }
                },
                tags: [handler.namespace || 'other']
            }
        }
    }

    return openApiSchema
}

type CodecTypes =
    t.TypeC<any> |
    t.StringC |
    t.NumberC |
    t.NullC |
    t.UndefinedC |
    t.UnionC<any> |
    t.BooleanC |
    t.ArrayC<any>

function codec2schema(codec: CodecTypes) {
    if (codec._tag === 'StringType') {
        return { type: 'string' }
    } else if (codec._tag === 'NumberType') {
        return { type: 'integer' }
    } else if (codec._tag === 'BooleanType') {
        return { type: 'boolean' }
    } else if (codec._tag === 'NullType') {
        return { type: 'null' }
    } else if (codec._tag === 'UndefinedType') {
        return { type: 'undefined' }
    } else if (codec._tag === 'UnionType') {
        return { oneOf: codec.types.map(t => codec2schema(t)) }
    } else if (codec._tag === 'InterfaceType') {
        let res = {
            type: 'object',
            properties: {}
        }
        for (let key of Object.keys(codec.props)) {
            res.properties[key] = codec2schema(codec.props[key])
        }
        return res
    } else if (codec._tag === 'ArrayType') {
        return {
            type: 'array',
            items: codec2schema(codec.type)
        }
    }
}