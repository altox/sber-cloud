import express, {Express} from "express";
import {AnyQueryHandler, AnyQueryHandlerSchema, buildHandlerPath} from "./queryHandler";
import {isLeft} from "fp-ts/Either";
import {AccessDeniedError, NotFoundError, RateLimitError, UserError} from "./errors";
import {APIContext} from "./Context";
import {Modules} from "../modules/Modules";
import {randomID} from "../utils/random";
import * as t from "io-ts";
import {Concurrency} from "./concurrency";

export const ApiErrorResponse = t.type({
    ok: t.boolean,
    id: t.string,
    errorCode: t.string,
    errorText: t.union([t.string, t.undefined]),
    shouldRetry: t.boolean
})

function buildErrorResponse(ctx: APIContext, e: Error) {
    let errorBody: t.TypeOf<typeof ApiErrorResponse>

    if (e instanceof UserError) {
        errorBody = {
            ok: false,
            id: ctx.id,
            errorCode: 'server_error',
            errorText: e.message,
            shouldRetry: true
        }
    } else if (e instanceof AccessDeniedError) {
        errorBody = {
            ok: false,
            id: ctx.id,
            errorCode: 'access_denied',
            errorText: undefined,
            shouldRetry: false
        }
    } else if (e instanceof NotFoundError) {
        errorBody = {
            ok: false,
            id: ctx.id,
            errorCode: 'not_found',
            errorText: undefined,
            shouldRetry: true
        }
    } else if (e instanceof RateLimitError) {
        errorBody = {
            ok: false,
            id: ctx.id,
            errorCode: 'rate_limit',
            errorText: undefined,
            shouldRetry: true
        }
    } else {
        errorBody = {
            ok: false,
            id: ctx.id,
            errorCode: 'server_error',
            errorText: undefined,
            shouldRetry: true
        }

        // Log unusual exceptions
        console.log('api_error', ctx.id, e)
    }

    return ApiErrorResponse.encode(errorBody)
}

function buildExpressHandler(handlerSchema: AnyQueryHandler) {
    return async (req: express.Request, response: express.Response) => {
        let { argsType, resType, handler } = handlerSchema

        let decoded = argsType.decode(req.body)
        if (isLeft(decoded)) {
            response.json({ ok: false, errorCode: 'invalid_request' })
            return
        }

        let context: APIContext = { id: randomID() }

        if (!Concurrency.Request.get('client-' + req.ip).tryTake()) {
            response.json(buildErrorResponse(context, new RateLimitError()))
            return
        }

        let token = req.header('X-Sber-Auth')
        if (token) {
            let auth = await Modules.Auth.authCheck(token!)
            if (auth) {
                context.tid = auth.tid
                context.uid = auth.uid
                context = { ...context, uid: auth.uid, tid: auth.tid }
            }
        }

        try {
            let res = await handler(context, decoded.right)
            response.json({ ok: true, response: resType.encode(res) })
            return
        } catch (e) {
            response.json(buildErrorResponse(context, e))
        }
    }
}

export const DEFAULT_API_VERSION = 1

export function initHandlers(app: Express, schema: Map<string, AnyQueryHandlerSchema>) {
    for (let item of schema.entries()) {
        let [name, handler] = item

        app.post(buildHandlerPath(name, handler, DEFAULT_API_VERSION), express.json(), buildExpressHandler(handler.handler))
    }
}