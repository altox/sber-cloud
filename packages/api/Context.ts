// Unique per API call
export type APIContext = {
    id: string // request id
    uid?: number
    tid?: number // token id
}

export type APIContextWithAuth = APIContext & Required<Pick<APIContext, 'tid' | 'uid'>>
