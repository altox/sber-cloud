import {SecIdFactory} from "../secure-id";

// Should be changed in production
const IDS_SECRET = 'DEV_IDS_SECRET'

export const IdsFactory = new SecIdFactory(IDS_SECRET)

export const IDs = {
    User: IdsFactory.createId('User'),
    VMOrder: IdsFactory.createId('VMOrder')
}