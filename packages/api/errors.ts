export class UserError extends Error {

}

export class NotFoundError extends Error {

}

export class AccessDeniedError extends Error {

}

export class RateLimitError extends Error {

}