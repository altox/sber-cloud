import {DataTypes, Model, Optional} from "sequelize";
import {DB} from "../db/initDatabase";

export interface AuthTokenInput {
    id: number
    uid: number
    token: string
    enabled: boolean
}

export class AuthToken extends Model<AuthTokenInput, Optional<AuthTokenInput, 'id'>> implements AuthTokenInput {
    id!: number
    uid!: number
    token!: string
    enabled!: boolean

    readonly createdAt!: Date
    readonly updatedAt!: Date
}

export function initAuthTokenModel() {
    AuthToken.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            uid: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            token: {
                type: new DataTypes.STRING(64),
                allowNull: false,
                unique: true
            },
            enabled: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: true
            }
        },
        {
            tableName: "auth_tokens",
            sequelize: DB,
            indexes: [
                {
                    unique: true,
                    fields: ['token']
                },
                { fields: ['uid'] },
                { fields: ['enabled'] }
            ]
        }
    )
}