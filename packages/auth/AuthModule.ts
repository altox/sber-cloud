import {Transaction} from "sequelize";
import {randomBytes, timingSafeEqual} from "crypto";
import {AuthToken} from "./AuthToken.model";
import {sha256} from "../utils/cryto";
import {User} from "../users/User.model";
import {UserError} from "../api/errors";
import {Modules} from "../modules/Modules";

export class AuthModule {
    start = async () => {
        // noop
    }

    createToken = async (tx: Transaction, uid: number) => {
        let token = randomBytes(32).toString('hex')

        let authToken = await AuthToken.create({
            uid,
            token,
            enabled: true
        }, { transaction: tx })

        return {
            id: authToken.id,
            token
        }
    }

    authorize = async (tx: Transaction, username: string, password: string) => {
        // Checking auth

        let passwordHash = sha256(password)
        let user = await User.findOne({ where: { userName: username }, transaction: tx })
        if (!user) {
            throw new UserError('User not found')
        }

        if (!timingSafeEqual(Buffer.from(passwordHash), Buffer.from(user.passwordHash))) {
            throw new UserError('Wrong password')
        }

        // Issue new token
        let authToken = await this.createToken(tx, user.id)

        return {
            uid: user.id,
            token: authToken.token,
            tid: authToken.id
        }
    }

    authCheck = async (token: string) => {
        let authToken = await AuthToken.findOne({ where: { token, enabled: true } })
        if (!authToken) {
            return null
        }
        return {
            tid: authToken.id,
            uid: authToken.uid
        }
    }

    revokeToken = async (tx: Transaction, tid: number) => {
        let authToken = await AuthToken.findByPk(tid, { transaction: tx })
        if (!authToken || !authToken.enabled) {
            return false
        }
        authToken.enabled = false
        await authToken.save({ transaction: tx })
        return true
    }

    revokeTokensExcept = async (tx: Transaction, uid: number, exceptTid: number) => {
        let tokens = await AuthToken.findAll({ where: { uid: uid, enabled: true }, transaction: tx })
        tokens = tokens.filter(t => t.id !== exceptTid)
        for (let token of tokens) {
            await Modules.Auth.revokeToken(tx, token.id)
        }
    }

    getUidByToken = async (tx: Transaction, token: number) => {
        let existing = await AuthToken.findOne({ where: { token, enabled: false }, transaction: tx })
        if (!existing) {
            return null
        }
        return existing.uid
    }
}