import * as t from "io-ts";

export const AuthTokenCodec = t.type({
    token: t.string
})