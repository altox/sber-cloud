import {APISchema, mutationHandler, withAuth} from "../api/queryHandler";
import * as t from "io-ts";
import {AuthTokenCodec} from "./codecs";
import {resolveUser, UserCodec} from "../users/codecs";
import {inTx} from "../db/inTx";
import {Modules} from "../modules/Modules";
import {User} from "../users/User.model";
import {IDs} from "../api/IDs";
import {BoolResponse, CommonCodecs} from "../api/commonCodecs";
import {withNamespace} from "../api/withAPIVersion";

const AuthorizeArgs = t.type({
    username: t.string,
    password: t.string
})

const AuthorizeRes = t.type({
    authToken: AuthTokenCodec,
    user: UserCodec
})

const SignUpArgs = t.type({
    username: t.string,
    password: t.string,
    firstName: t.string,
    lastName: t.string,
})

export const Handlers: APISchema = withNamespace(1, 'auth', {
    authorize: mutationHandler(AuthorizeArgs, AuthorizeRes, async (ctx, args) => {
        return await inTx(async tx => {
            let auth = await Modules.Auth.authorize(tx, args.username, args.password)
            let user = (await User.findByPk(auth.uid))!

            return {
                authToken: { token: auth.token },
                user: resolveUser(user)
            }
        })
    }),
    signUp: mutationHandler(SignUpArgs, AuthorizeRes, async (ctx, args) => {
        return await inTx(async tx => {
            let user = await Modules.Users.createUser(tx, {
                firstName: args.firstName,
                lastName: args.lastName,
                userName: args.username,
                password: args.password
            })

            let auth = await Modules.Auth.createToken(tx, user.id)

            return {
                authToken: { token: auth.token },
                user: resolveUser(user)
            }
        })
    }),
    terminateCurrentSession: mutationHandler(CommonCodecs, BoolResponse, withAuth(async (ctx, args) => {
        return inTx(async tx => ({
            ok: await Modules.Auth.revokeToken(tx, ctx.tid)
        }))
    })),
    terminateOtherSessions: mutationHandler(CommonCodecs, BoolResponse, withAuth(async (ctx, args) => {
        return inTx(async tx => {
            await Modules.Auth.revokeTokensExcept(tx, ctx.uid, ctx.tid)
            return { ok: true }
        })
    })),
})