import * as t from "io-ts";
import {resolveUser, UserCodec} from "../users/codecs";
import {VMOrder} from "./VMOrder.model";
import {IDs} from "../api/IDs";
import {User} from "../users/User.model";

export const VMOrderCodec = t.type({
    id: t.string,
    orderNumber: t.number,
    client: UserCodec,
    manager: t.union([UserCodec, t.null]),
    vCPU: t.number,
    vRAM: t.number,
    vHDD: t.number,
    description: t.string,
    state: t.string
})

export const resolveVMOrder = async (order: VMOrder) => ({
    id: IDs.VMOrder.serialize(order.id),
    orderNumber: order.id,
    client: resolveUser((await User.findByPk(order.clientUid))!),
    manager: order.managerUid ? resolveUser((await User.findByPk(order.managerUid))!) : null,
    vCPU: order.vCPU,
    vRAM: order.vRAM,
    vHDD: order.vHDD,
    description: order.description,
    state: order.state
})