import {APISchema, mutationHandler, queryHandler, withAuth, withPermission} from "../api/queryHandler";
import {withNamespace} from "../api/withAPIVersion";
import {BoolResponse, paginatedResponse, PaginationArgs, SingleIDArgs} from "../api/commonCodecs";
import {Modules} from "../modules/Modules";
import {resolveVMOrder, VMOrderCodec} from "./codecs";
import {VMOrder} from "./VMOrder.model";
import {IDs} from "../api/IDs";
import {AccessDeniedError, NotFoundError, UserError} from "../api/errors";
import * as t from "io-ts";
import {Op} from "sequelize";

export const OrdersListResponse = paginatedResponse(VMOrderCodec)

export const VMOrderCreateArgs = t.type({
    vCPU: t.number,
    vRAM: t.number,
    vHDD: t.number,
    description: t.string
})

export const Handlers: APISchema =  withNamespace(1, 'cloud', {
    // Queries
    getOrder: queryHandler(SingleIDArgs, VMOrderCodec, withAuth(async (ctx, args) => {
        let oid = IDs.VMOrder.parse(args.id)

        let permissions = await Modules.Users.fetchUserPermissions(ctx.uid)

        // User needs to be at least customer
        if (!permissions.includes('customer')) {
            throw new AccessDeniedError()
        }

        let order = await VMOrder.findByPk(oid)

        if (!order) {
            throw new NotFoundError()
        }

        let canViewAnyOrder = permissions.includes('manager') || permissions.includes('super-user')

        if (!canViewAnyOrder && order.clientUid !== ctx.uid) {
            throw new AccessDeniedError()
        }

        return await resolveVMOrder(order)
    })),
    getMyOrders: queryHandler(PaginationArgs, OrdersListResponse, withPermission('customer', async (ctx, args) => {
        let orders = await VMOrder.findAll({
            where: {
                clientUid: ctx.uid,
                ...(args.after ? {
                    id: {
                        [Op.gt]: IDs.VMOrder.parse(args.after)
                    }
                } : {})
            },
            limit: args.first + 1
        })

        return {
            items: await Promise.all(orders.slice(0, args.first).map(o => resolveVMOrder(o))),
            cursor: orders.length > args.first ? IDs.VMOrder.serialize(orders[orders.length - 2].id) : undefined
        }
    })),
    getAllOrders: queryHandler(PaginationArgs, OrdersListResponse, withPermission('manager', async (ctx, args) => {
        let orders = await VMOrder.findAll({
            where: {
                ...(args.after ? {
                    id: {
                        [Op.gt]: IDs.VMOrder.parse(args.after)
                    }
                } : {})
            },
            limit: args.first + 1
        })

        return {
            items: await Promise.all(orders.slice(0, args.first).map(o => resolveVMOrder(o))),
            cursor: orders.length > args.first ? IDs.VMOrder.serialize(orders[orders.length - 2].id) : undefined
        }
    })),

    // Mutation
    createOrder: mutationHandler(VMOrderCreateArgs, VMOrderCodec, withPermission('customer', async (ctx, args) => {
        // Validate args
        if (args.vCPU < 0 || args.vCPU > 100) {
            throw new UserError('CPU count should be from 1 to 100');
        }
        if (args.vRAM < 1024 || args.vRAM > 1024*64) {
            throw new UserError('RAM should be from 1024 to 1024*64');
        }
        if (args.vHDD < 1 || args.vHDD > 10000) {
            throw new UserError('HDD should be from 1 to 10000');
        }

        let order = await VMOrder.create({
            clientUid: ctx.uid,
            managerUid: null,
            vCPU: args.vCPU,
            vRAM: args.vRAM,
            vHDD: args.vHDD,
            description: args.description,
            state: 'pending',
        })

        return resolveVMOrder(order)
    })),
    declineOrder: mutationHandler(SingleIDArgs, BoolResponse, withPermission('manager', async (ctx, args) => {
        let order = await VMOrder.findByPk(IDs.VMOrder.parse(args.id))
        if (!order) {
            throw new AccessDeniedError()
        }
        if (order.state !== 'pending' && order.state !== 'in-progress') {
            throw new UserError('Insufficient state')
        }
        order.state = 'declined'
        order.managerUid = ctx.uid
        await order.save()

        return { ok: true }
    })),
    acceptOrder: mutationHandler(SingleIDArgs, BoolResponse, withPermission('manager', async (ctx, args) => {
        let order = await VMOrder.findByPk(IDs.VMOrder.parse(args.id))
        if (!order) {
            throw new AccessDeniedError()
        }
        if (order.state !== 'pending' && order.state !== 'in-progress') {
            throw new UserError('Insufficient state')
        }
        order.state = 'done'
        order.managerUid = ctx.uid
        await order.save()

        return { ok: true }
    })),
})