import {DataTypes, Model, Optional} from "sequelize";
import {DB} from "../db/initDatabase";

type VMOrderState =
    'pending' |
    'in-progress' |
    'declined' |
    'done'

export interface VMOrderInput {
    id: number
    clientUid: number
    managerUid: number|null
    vCPU: number // number of cores
    vRAM: number // RAM size in megabytes
    vHDD: number // HDD size in gigabytes
    description: string
    state: VMOrderState
}

export class VMOrder extends Model<VMOrderInput, Optional<VMOrderInput, 'id'>> implements VMOrderInput {
    id!: number
    clientUid: number
    managerUid: number|null
    vCPU: number
    vRAM: number
    vHDD: number
    description: string
    state: VMOrderState

    readonly createdAt!: Date
    readonly updatedAt!: Date
}

export function initVMOrderModel() {
    VMOrder.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            clientUid: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            managerUid: {
                type: DataTypes.INTEGER,
                allowNull: true,
            },
            vCPU: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            vRAM: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            vHDD: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            description: {
                type: new DataTypes.TEXT(),
                allowNull: false
            },
            state: {
                type: new DataTypes.ENUM('pending', 'in-progress', 'done'),
                allowNull: false
            }
        },
        {
            tableName: "vm_orders",
            sequelize: DB,
            indexes: [
                {
                    fields: ['clientUid']
                }
            ]
        }
    )
}