import {initApi} from "../api/initApi";
import {initDatabase, syncDatabase} from "../db/initDatabase";
import {initModules} from "../modules/Modules";

async function bootstrap() {
    console.log('starting server')

    // Order matters!

    console.log('init db')

    await initDatabase()

    console.log('sync db')
    await syncDatabase()

    console.log('init modules')
    await initModules()

    console.log('init api')
    await initApi()
}

bootstrap()