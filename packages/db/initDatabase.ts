import {Sequelize} from "sequelize";
import {initUserModel} from "../users/User.model";
import {initAuthTokenModel} from "../auth/AuthToken.model";
import {initVMOrderModel} from "../cloud/VMOrder.model";

export let DB: Sequelize

const DB_NAME = 'sber-cloud'

export async function initDatabase() {
    DB = new Sequelize('postgres://postgres:postgres@db:5432')
    await DB.authenticate()
    try {
        await DB.query(`CREATE DATABASE "${DB_NAME}";`)
    } catch (e) {
        // noop
    }
    await DB.close()

    DB = new Sequelize('postgres://postgres:postgres@db:5432/' + DB_NAME)
    await DB.authenticate()
}

export async function syncDatabase() {
    initAuthTokenModel()
    initUserModel()
    initVMOrderModel()

    await DB.sync()
}