import {Transaction} from "sequelize";
import {DB} from "./initDatabase";

export async function inTx<T>(cb: (tx: Transaction) => Promise<T>): Promise<T> {
    return await DB.transaction(cb)
}