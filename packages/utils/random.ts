import {randomBytes} from "crypto";

export const randomID = () => randomBytes(32).toString('hex')