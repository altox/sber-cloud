export class CachedFactory<T> {
    #emitter: (key: string) => T
    #cache = new Map<string, T>()

    constructor(emitter: (key: string) => T) {
        this.#emitter = emitter
    }

    get = (key: string) => {
        let ex = this.#cache.get(key)
        if (ex) {
            return ex
        }
        let val = this.#emitter(key)
        this.#cache.set(key, val)
        return val
    }
}