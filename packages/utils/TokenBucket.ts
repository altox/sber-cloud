export class TokenBucket {
    #capacity: number
    #refillDelay: number
    #refillAmount: number
    #tokens: number
    #lastRefill: number

    constructor(opts: { capacity: number, refillDelay: number, refillAmount: number }) {
        this.#capacity = opts.capacity
        this.#refillDelay = opts.refillDelay
        this.#refillAmount = opts.refillAmount
        this.#tokens = this.#capacity
        this.#lastRefill = Date.now()
    }

    tryTake = () => {
        this.#refillIfNeeded()
        if (this.#tokens < 1) {
            return false
        }

        this.#tokens--
        return true
    }

    #refillIfNeeded = () => {
        if (this.#tokens >= this.#capacity) {
            return
        }
        let refillAmount = Math.floor(this.#refillAmount * ((Date.now() - this.#lastRefill) / this.#refillDelay))
        if (refillAmount <= 0) {
            return;
        }

        if ((refillAmount + this.#tokens) > this.#capacity) {
            refillAmount = this.#capacity - this.#tokens
        }

        this.#tokens += refillAmount
        this.#lastRefill += refillAmount * this.#refillDelay
    }
}