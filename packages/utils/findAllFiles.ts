import {lstatSync, readdirSync} from "fs";

export function findAllFiles(path: string, condition: (src: string) => boolean) {
    let res: string[] = []
    let nodes = readdirSync(path)

    for (let node of nodes) {
        let fullPath = path + node

        if (lstatSync(fullPath).isDirectory()) {
            res.push(...findAllFiles(fullPath + '/', condition))
        } else {
            if (condition(fullPath)) {
                res.push(fullPath)
            }
        }
    }

    return res
}