import {createHash} from "crypto";

export const sha256 = (src: string) => createHash('sha256').update(src).digest('hex')