import {UsersModule} from "../users/UsersModule";
import {AuthModule} from "../auth/AuthModule";
import {CloudModule} from "../cloud/CloudModule";

export let Modules: ModulesImpl

class ModulesImpl {
    public Users!: UsersModule
    public Auth!: AuthModule
    public Cloud!: CloudModule

    constructor() {

    }
}

export async function initModules() {
    let modules = new ModulesImpl()

    let users = new UsersModule()
    await users.start()
    modules.Users = users

    let auth = new AuthModule()
    await auth.start()
    modules.Auth = auth

    let cloud = new CloudModule()
    await cloud.start()
    modules.Cloud = cloud

    Modules = modules
}